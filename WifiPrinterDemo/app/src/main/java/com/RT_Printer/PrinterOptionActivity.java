package com.RT_Printer;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import com.RT_Printer.WIFI.WifiPrintDriver;
import com.RT_Printer.util.Utils;
import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class PrinterOptionActivity extends Activity{
	private Button mBtnBack = null;
	private Button mBtnPrintText = null;
	private Button mBtnPrintImage = null;
	private Button mBtnPrint1DBarcode = null;
	private Button mBtnPrintTicket = null;
	private Button mBtnPrintTable = null;
	private EditText m1DBarcodeContent = null;
	private WifiPrintDriver wifiSocket = null;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.printer_option);
        InitUIControl();
        if (wifiSocket == null) 
        	wifiSocket = new WifiPrintDriver(this, mHandler);
    }
	// The Handler that gets information back from the BluetoothChatService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {}
        }
    };



	@Override
	protected void onResume() {
//		this.mBloothPrinter = BloothPrinterActivity.mBloothPrinter;
		super.onResume();
	}


	private String getLanguageEnv() {  
	       Locale l = Locale.getDefault();  
	       String language = l.getLanguage();  
	       String country = l.getCountry().toLowerCase();  
	       if ("zh".equals(language)) {  
	           if ("cn".equals(country)) {  
	               language = "zh-CN";  
	           } else if ("tw".equals(country)) {  
	               language = "zh-TW";  
	           }  
	       } else if ("pt".equals(language)) {  
	           if ("br".equals(country)) {  
	               language = "pt-BR";  
	           } else if ("pt".equals(country)) {  
	               language = "pt-PT";  
	           }  
	       }  
	       return language;  
	}  
	
	
	private void InitUIControl(){
    	mBtnBack = (Button)findViewById(R.id.btn_back);
    	mBtnBack.setOnClickListener(mBtnBackOnClickListener);
    	mBtnPrintText = (Button)findViewById(R.id.btn_print_text);
    	mBtnPrintText.setOnClickListener(mBtnPrintTextOnClickListener);
    	mBtnPrintImage = (Button)findViewById(R.id.btn_print_image);
    	mBtnPrintImage.setOnClickListener(mBtnPrintImageOnClickListener);
    	mBtnPrint1DBarcode = (Button)findViewById(R.id.btn_print_barcode);
    	mBtnPrint1DBarcode.setOnClickListener(mBtnPrint1DBarcodeOnClickListener);
    	mBtnPrintTicket = (Button)findViewById(R.id.btn_print_smallticket);
    	mBtnPrintTicket.setOnClickListener(mBtnPrintTicketOnClickListener);
    	mBtnPrintTable = (Button)findViewById(R.id.btn_print_table);
    	mBtnPrintTable.setOnClickListener(mBtnPrintTableOnClickListener);
    	m1DBarcodeContent = (EditText)findViewById(R.id.edt_barcode_content);
    }

	OnClickListener mBtnBackOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			finish();
		}
	};

	OnClickListener mBtnPrintTextOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(wifiSocket.IsNoConnection()){
				return;
			}
			wifiSocket.Begin();

			String tmpString = PrinterOptionActivity.this.getResources().getString(R.string.print_text_content);
			wifiSocket.WIFI_Write(tmpString);
			wifiSocket.WIFI_Write("\r\n");
			wifiSocket.LF();
			wifiSocket.LF();
		}
	};

	OnClickListener mBtnPrintImageOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(wifiSocket.IsNoConnection()){
				return;
			}
			InputStream in = null;
			try {
				in = getResources().getAssets().open("Rongta.jpg");
			} catch (IOException e) {
				e.printStackTrace();
			}

/*			BufferedInputStream bis = new BufferedInputStream(in);
			Bitmap bitmap = BitmapFactory.decodeStream(bis);
			//??????????????
			byte[] start = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1B, 0x40, 0x1B, 0x33, 0x00 };
			mBloothPrinter.Begin();
			mBloothPrinter.WIFI_Write(start, start.length);
			//??????????
			byte[] byteImage = Utils.getReadBitMapBytes(bitmap);		
			mBloothPrinter.WIFI_Write(byteImage, byteImage.length);
			//?????????? ????????????????????????????
			byte[] end = { 0x1d, 0x4c, 0x1f, 0x00 };
			mBloothPrinter.WIFI_Write(end, end.length);
*/			
			wifiSocket.printImage();
			
		}
	};

	OnClickListener mBtnPrint1DBarcodeOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(wifiSocket.IsNoConnection()){
				return;
			}
			wifiSocket.Begin();
			String print1DBarcodeStr = m1DBarcodeContent.getText().toString();
        	int len = print1DBarcodeStr.length();
        	if(len > 16){
        		String tmpString = PrinterOptionActivity.this.getResources().getString(R.string.barcode_input_hint);
    			Utils.ShowMessage(PrinterOptionActivity.this, tmpString);
        		return;
        	}
//        	for(int i=0; i<len; i++){
//        		if(print1DBarcodeStr.charAt(i)<'0' || print1DBarcodeStr.charAt(i)>'9'){
//        			//Utils.ShowMessage(PrinterOptionActivity.this, "????????????0??9?????????!");
//        			String tmpString = PrinterOptionActivity.this.getResources().getString(R.string.barcode_input_hint);
//        			Utils.ShowMessage(PrinterOptionActivity.this, tmpString);
//            		return;
//            	}
//        	}
        	wifiSocket.AddCodePrint(WifiPrintDriver.Code128_B, print1DBarcodeStr);
			}
	};

	OnClickListener mBtnPrintTicketOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(wifiSocket.IsNoConnection()){
				return;
			}
			
			String tmpString1 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content1);
			String tmpString2 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content2);
			String tmpString3 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content3);
			String tmpString4 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content4);
			String tmpString5 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content5);
			String tmpString6 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content6);
			String tmpString7 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content7);
			String tmpString8 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content8);
			String tmpString9 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content9);
			String tmpString10 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content10);
			String tmpString11 = PrinterOptionActivity.this.getResources().getString(R.string.print_smallticket_content11);
			String tmpString12 = PrinterOptionActivity.this.getResources().getString(R.string.print_ticket_line1);
			String tmpString13 = PrinterOptionActivity.this.getResources().getString(R.string.print_ticket_line2);
			
			
			wifiSocket.Begin();
			wifiSocket.LF();
			wifiSocket.LF();
			wifiSocket.SetAlignMode((byte)1);//????
			wifiSocket.SetLineSpacing((byte)50);	
			wifiSocket.SetFontEnlarge((byte)0x11);//?????????		
			wifiSocket.WIFI_Write(tmpString1);
			wifiSocket.LF();
			wifiSocket.LF();
			wifiSocket.LF();
			wifiSocket.SetAlignMode((byte)0);//?????		
			wifiSocket.SetFontEnlarge((byte)0x00);//???????????
			wifiSocket.WIFI_Write(tmpString2);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString3);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString4);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString12);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString5);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString12);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString6);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString7);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString12);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString8);
			wifiSocket.LF();
			wifiSocket.SetFontEnlarge((byte)0x11);//?????????	
			wifiSocket.WIFI_Write(tmpString9);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString13);
			wifiSocket.LF();
			wifiSocket.WIFI_Write(tmpString10);
			wifiSocket.LF();
			wifiSocket.SetFontEnlarge((byte)0x00);//???????????	
			wifiSocket.WIFI_Write(tmpString11);
			wifiSocket.LF();
			wifiSocket.LF();
			wifiSocket.LF();
			wifiSocket.LF();
			wifiSocket.LF();
		}
	};

	OnClickListener mBtnPrintTableOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if(wifiSocket.IsNoConnection()){
				return;
			}
			
			String tmpString1 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content1);
			String tmpString2 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content2);
			String tmpString3 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content3);
			String tmpString4 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content4);
			String tmpString5 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content5);
			String tmpString6 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content6);
			String tmpString7 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content7);
			String tmpString8 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content8);
			String tmpString9 = PrinterOptionActivity.this.getResources().getString(R.string.print_table_content9);

		    String language = getLanguageEnv();//??????????????
		    if (language != null  
		            && (language.trim().equals("zh-CN") || language.trim().equals("zh-TW")))  
		    {//????
		    	wifiSocket.Begin();
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);	//???????
				wifiSocket.WIFI_Write(String.format(tmpString1),true);
				wifiSocket.WIFI_Write(String.format(tmpString2),true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01},3);	//?????????
				wifiSocket.WIFI_Write(String.format(tmpString3),true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);	//???????
				wifiSocket.WIFI_Write(String.format(tmpString4),true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01},3);	//?????????
				wifiSocket.WIFI_Write(String.format(tmpString5), true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01},3);	//???????
				wifiSocket.WIFI_Write(String.format(tmpString6),true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01},3);	//?????????
				wifiSocket.WIFI_Write(String.format(tmpString7),true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01},3);	//???????
				wifiSocket.WIFI_Write(String.format(tmpString8),true);
				wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x00},3);	//?????????
				wifiSocket.WIFI_Write(String.format(tmpString9),true);
				wifiSocket.LF();
				wifiSocket.LF();
		    }
		    else  
		    {// ???
		    	wifiSocket.Begin();
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);	//???????
				// ?????
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xDA},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x51}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC2},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x71}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC2},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x11}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC2},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x51}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xBF,0x0A},2);// ??
				// ?????
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);// ??
				wifiSocket.WIFI_Write(String.format("From  "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);// ??
				wifiSocket.WIFI_Write(String.format("Shanghai"),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);// ??
				wifiSocket.WIFI_Write(String.format("To"),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);// ??
				wifiSocket.WIFI_Write(String.format("Xiamen"),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3,0x0A},2);
				// ??????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xC3},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x51}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC5},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x31}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC2},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x21}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC1},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC2},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC1},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x51}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);// ??
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xB4,0x0A},2);// ??
				//??????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("Amount"),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format(" 1  "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("No. "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("5555555 "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3,0x0A},2);
				//??????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xC3},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x51}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC1},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x21}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC2},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC1},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x31}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC1},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x71}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xB4,0x0A},2);	
				//??????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("Addressee "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("Sun Jun       "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3,0x0A},2);
				//??????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xC3},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x41}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4,(byte)0xC4},2);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC5},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x61}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4,(byte)0xC4},2);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xB4,0x0A},2);    	
				//?????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("Pickup by "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3},1);
				wifiSocket.WIFI_Write(String.format("              "),true);
				wifiSocket.WIFI_Write(new byte[]{(byte)0xB3,0x0A},2);
				//?????
				wifiSocket.WIFI_Write(new byte[]{(byte)0xC0},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x41}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4,(byte)0xC4},2);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC1},1);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x61}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xC4,(byte)0xC4},2);
		    	wifiSocket.WIFI_Write(new byte[]{0x1d,0x21,0x01}, 3);
		    	wifiSocket.WIFI_Write(new byte[]{(byte)0xD9,0x0A},2);  
				//
				wifiSocket.LF();
				wifiSocket.LF();
		    }

		}
	};
}
