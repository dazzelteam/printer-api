
package com.RT_Printer;

import com.RT_Printer.util.Utils;
import com.RT_Printer.WIFI.WifiPrintDriver;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class RTPrinterActivity extends Activity {
	// Debugging
    private static final String TAG = "WIFIPrinterActivity";
    private static final boolean D = true;
    
    // Message types sent from the BluetoothChatService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_TOAST = 4;
    
    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";
    public static int revBytes=0;   
    
	private Button mBtnConnetWifiDevice = null;
	private Button mBtnQuit = null;
	private Button mBtnPrint = null;
	private Button mBtnPrintOption = null;
	private Button mBtnTest = null;
	private EditText mIPAdrressAndPort = null;
	private EditText mPrintContent = null;
	private CheckBox mBeiKuan = null;
	private CheckBox mUnderline = null;
	private CheckBox mBold = null;
	private CheckBox mBeiGao = null;
	private CheckBox mMinifont = null;
	private CheckBox mHightlight = null;
	private WifiPrintDriver wifiSocket = null;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		Log.e(TAG,"onCreate begin");
        if(D) Log.e(TAG, "+++ ON CREATE +++");
        setContentView(R.layout.main);
        setTitle(R.string.bluetooth_unconnected);
        InitUIControl();
        if (wifiSocket == null) 
        	wifiSocket = new WifiPrintDriver(this, mHandler);
    }
    
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

			Log.d(TAG,"mHandler begin");

			Log.d(TAG,""+msg.what);

			switch (msg.what) {



            case MESSAGE_STATE_CHANGE:
                if(D) Log.i(TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                switch (msg.arg1) {}
                break;
            case MESSAGE_WRITE:

				Log.d(TAG,"Inside Message_Write");

                break;
            case MESSAGE_READ:
            	String Msg = null;
            	byte[] readBuf = (byte[]) msg.obj;
				Log.d(TAG,Msg);
                if(D) Log.i(TAG, "readBuf[0]:"+readBuf[0] + "  revBytes:"+revBytes);
                
                Msg = "";
                for(int i=0;i<revBytes;i++)
                {
                	Msg = Msg +" 0x";
                	Msg = Msg + Integer.toHexString(readBuf[i]);
                }
				Log.d(TAG,Msg);
                DisplayToast(Msg);

                break;
            case MESSAGE_TOAST:
                break;
            }
        }
    };

	private void InitUIControl(){
		Log.d(TAG,"InitUIControl begin");


    	mBtnQuit = (Button)findViewById(R.id.btn_quit);
    	mBtnQuit.setOnClickListener(mBtnQuitOnClickListener);
    	mBtnConnetWifiDevice = (Button)findViewById(R.id.btn_connect_to_ip);
    	mBtnConnetWifiDevice.setOnClickListener(mBtnConnetWifiDeviceOnClickListener);
    	mBtnPrint = (Button)findViewById(R.id.btn_print);
    	mBtnPrint.setOnClickListener(mBtnPrintOnClickListener);
    	mBtnPrintOption = (Button)findViewById(R.id.btn_option);
    	mBtnPrintOption.setOnClickListener(mBtnPrintOptionOnClickListener);
    	mBtnTest = (Button)findViewById(R.id.btn_test);
    	mBtnTest.setOnClickListener(mBtnTestOnClickListener);
    	mIPAdrressAndPort = (EditText)findViewById(R.id.edt_ip_address);
    	mPrintContent = (EditText)findViewById(R.id.edt_print_content);
    	mBeiKuan = (CheckBox)findViewById(R.id.checkbox_beikuan);
    	mUnderline = (CheckBox)findViewById(R.id.checkbox_underline);
    	mBold = (CheckBox)findViewById(R.id.checkbox_bold);
    	mBeiGao = (CheckBox)findViewById(R.id.checkbox_beigao);
    	mMinifont = (CheckBox)findViewById(R.id.checkbox_minifont);
    	mHightlight = (CheckBox)findViewById(R.id.checkbox_hightlight);
    }
    @Override
	protected void onResume() {
		super.onResume();
		if(D) Log.e(TAG, "+ ON RESUME +");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (wifiSocket != null) wifiSocket.stop();
		if(D) Log.e(TAG, "--- ON DESTROY ---");
	}

	OnClickListener mBtnQuitOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (wifiSocket != null) wifiSocket.stop();
			finish();
		}
	};

	OnClickListener mBtnConnetWifiDeviceOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {


		}
	};

	private void connectToPrinter(){
		Log.d(TAG,"mBtnConnetWifiDeviceOnClickListener begin");

		String tmpStr = mIPAdrressAndPort.getText().toString();
		String ipAddress = "";
		String tmpPort = "";
		int port = 9100;
		String[] strings = Utils.StringSplit(tmpStr, ":");
		ipAddress = strings[0];
		tmpPort = strings[1];
		port = Integer.parseInt(tmpPort);
		// ????WIFI
		if(!wifiSocket.WIFISocket(ipAddress, port))
			RTPrinterActivity.this.setTitle(R.string.wifi_connect_fail);
		else
			RTPrinterActivity.this.setTitle(R.string.wifi_connect_sucess);
	}

	OnClickListener mBtnPrintOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			connectToPrinter();

			Log.d(TAG,"mBtnPrintOnClickListener begin");

			if(wifiSocket.IsNoConnection()){
				return;
			}
			wifiSocket.Begin();
			if(mBeiKuan.isChecked()){
				wifiSocket.SetFontEnlarge((byte)0x10);
			}
			if(mBeiGao.isChecked()){
				wifiSocket.SetFontEnlarge((byte)0x01);
			}
			if(mUnderline.isChecked()){
				wifiSocket.SetUnderline((byte)0x02);
			}
			if(mBold.isChecked()){
				wifiSocket.SetBold((byte)0x01);//????
			}
			if(mMinifont.isChecked()){
				wifiSocket.SetCharacterFont((byte)0x01);
			}
			if(mHightlight.isChecked()){
				wifiSocket.SetBlackReversePrint((byte)0x01);
			}
			String tmpContent = mPrintContent.getText().toString();
			wifiSocket.WIFI_Write(tmpContent);
			wifiSocket.WIFI_Write("\r\n");

			Log.d(TAG,"Printing completed");

			if(wifiSocket!= null){
				Log.d(TAG,"printer disconnected");
				wifiSocket.stop();
				RTPrinterActivity.this.setTitle("Printer connection closed");
			}

		}
	};

	OnClickListener mBtnPrintOptionOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent();
			intent.setClass(RTPrinterActivity.this, PrinterOptionActivity.class);
			//intent.putExtra("mBloothPrinter", mBloothPrinter);
			startActivity(intent);
		}
	};


	OnClickListener mBtnTestOnClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Log.d(TAG,"onClickListener begin");


			if(wifiSocket.IsNoConnection()){
				return;
			}
			Log.d(TAG,"connection begin");
			wifiSocket.Begin();
			String tmpContent = mPrintContent.getText().toString();
			String tmpContent1 = str2HexStr(tmpContent);
			byte[] send = HexString2Bytes(tmpContent1);
			wifiSocket.WIFI_Write(send);

		}
	};

	//??????
    public void showMessage(String str)
    {
        Toast.makeText(this,str, Toast.LENGTH_LONG).show();
    }//showMessage
    
    // ???Toast
	public void DisplayToast(String str)
	{
		Toast toast = Toast.makeText(this, str, Toast.LENGTH_SHORT);
		//????toast????????
		toast.setGravity(Gravity.TOP, 0, 100);
		//?????Toast
		toast.show();
	}//DisplayToast
	
	/**   
	 * ??????????????????????  
//	 * @param String str ???????ASCII?????
	 * @return String ???Byte???????????: [61 6C 6B]  
	 */      
	public static String str2HexStr(String str)    
	{      
	    char[] chars = "0123456789ABCDEF".toCharArray();      
	    StringBuilder sb = new StringBuilder("");    
	    byte[] bs = str.getBytes();      
	    int bit;      
	        
	    for (int i = 0; i < bs.length; i++)    
	    {      
	    	if(bs[i] >= 'a')
	    	    	bit = (bs[i]-87) & 0x0f;  
	    	else if(bs[i] >= 'A')
	    		bit = (bs[i]-55) & 0x0f;
	    	else
	    		bit = (bs[i]-0x30) & 0x0f;
	    	sb.append(chars[bit]);
	    	
	        i = i + 1;
	        
	        if(bs[i] >= 'a')
    	    	bit = (bs[i]-87) & 0x0f;  
	    	else if(bs[i] >= 'A')
	    		bit = (bs[i]-55) & 0x0f;
	    	else
	    		bit = (bs[i]-0x30) & 0x0f;
	        sb.append(chars[bit]);
	        //sb.append(' ');   
	    }      
	    return sb.toString().trim();      
	}    
	private final static byte[] hex = "0123456789ABCDEF".getBytes();  
	private static int parse(char c) {  
	    if (c >= 'a')  
	        return (c - 'a' + 10) & 0x0f;  
	    if (c >= 'A')  
	        return (c - 'A' + 10) & 0x0f;  
	    return (c - '0') & 0x0f;  
	}  
	// ????????????????????????  
	public static String Bytes2HexString(byte[] b) {  
	    byte[] buff = new byte[2 * b.length];  
	    for (int i = 0; i < b.length; i++) {  
	        buff[2 * i] = hex[(b[i] >> 4) & 0x0f];  
	        buff[2 * i + 1] = hex[b[i] & 0x0f];  
	    }  
	    return new String(buff);  
	}  
	// ??????????????????????????  
	public static byte[] HexString2Bytes(String hexstr) {  
	    byte[] b = new byte[hexstr.length() / 2];  
	    int j = 0;  
	    for (int i = 0; i < b.length; i++) {  
	        char c0 = hexstr.charAt(j++);  
	        char c1 = hexstr.charAt(j++);  
	        b[i] = (byte) ((parse(c0) << 4) | parse(c1));  
	    }  
	    return b;  
	}  

}